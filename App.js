import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import UserDetails from "./src/userDetails";
import UserList from "./src/userList";

const Stack = createStackNavigator(); 

export default function App() {
  return (
    <NavigationContainer>

      <Stack.Navigator initialRouteName="ListaDeUsers">
        <Stack.Screen name="ListaDeUsers" component={UserList}/>
        <Stack.Screen name="DetalhesDosUsuários" component={UserDetails} options={{title:'Informações dos Users'}}/>
      </Stack.Navigator>

    </NavigationContainer>
  );
}

