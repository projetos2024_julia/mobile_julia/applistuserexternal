import React from "react";
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

const UserDetails = ({ route }) => {
    const {user} = route.params;

    return(
        <View>
            <Text> Detalhes do User:</Text>
            <Text> Nome: {user.name}</Text>
            <Text> id: {user.id}</Text>
            <Text> UserName: {user.username}</Text>
            <Text> Email: {user.email}</Text>
            <Text> Telefone: {user.phone}</Text>
            <Text> WebSite: {user.website}</Text>
            <Text> Cidade: {user.address.city}</Text>
            <Text> Nome da Companhia: {user.company.name}</Text>
            <Text> Frase da Empresa: {user.company.catchPhrase}</Text>
            <Text> BS da companhia: {user.company.bs}</Text>
        </View>
    )
}
export default UserDetails;